package com.example.prueba

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.VerticalAlignmentLine
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.prueba.ui.theme.PruebaTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PruebaTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    EjClase()
                }
            }
        }
    }
}

@Composable
fun Huerto(){
    var num by remember {
        mutableStateOf(0)
    }
    var cont by remember {
        mutableStateOf(1)
    }
    var id by remember {
        mutableStateOf(R.drawable.foto_huerto)
    }



    Column(verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter = painterResource(id = id),
            contentDescription = "Huerto",
            modifier = Modifier.size(300.dp),
            contentScale = ContentScale.FillWidth)

        Button(

            onClick = {
                if (cont == 1){
                    id = R.drawable.huertolleno
                    cont = 2
                }else if(cont == 2){
                    id = R.drawable.huertorecogido
                    cont = 3
                    num += (1..5).random()
                }else if (cont == 3){
                    id = R.drawable.foto_huerto
                    cont = 1
                }

            })
        {
            Text(text = "Pulsa")
        }
        Text("Ganancias: " + num.toString() + "$",)

    }

}
@Preview(showBackground = true)
@Composable
fun EjClase() {
    PruebaTheme {
        Huerto()
    }
}
@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
            text = "Hello $name!",
            modifier = modifier
    )
}


@Composable
fun  PruebaBotones(){
    var num by remember {
        mutableStateOf(1)
    }
    Column() {
        Button(
            onClick = {
                num = (1..10).random()
            })
        {
            Text(text = "Botón")
        }
        Text(text = num.toString())
    }


}

@Composable
fun cuadricula(modifier: Modifier = Modifier) {
    var imagen = painterResource(id = R.drawable.kikorivera)
    Image(
        painter = imagen,
        contentDescription = "kiko rivera",
        contentScale = ContentScale.Crop,
        alpha = 0.6f
    )

    Column() {
        Text(
            text = stringResource(id = R.string.saludo),
        )
        Row() {
            Text(
                text = "1",
                modifier = modifier,
                color = Color.Red
            )
            Text(
                text = "2",
                modifier = modifier,
                color = Color.Blue
            )
            Text(
                text = "3",
                modifier = modifier,
                color = Color.Red
            )
        }
        Row() {
            Text(
                text = "4",
                modifier = modifier,
                color = Color.Blue
            )
            Text(
                text = "5",
                modifier = modifier,
                color = Color.Red
            )
            Text(
                text = "6",
                modifier = modifier,
                color = Color.Blue
            )
        }
    }


}